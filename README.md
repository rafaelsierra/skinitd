# SKINITD

## An attempt for a super light init process, written in Rust, for usage in containerized environments

### What problems does this software solve

Containers are supposed to run only one command ( as a rule of thumb ), but when they do, this command runs with PID 1, and the following can be observed :

- PID 1 is the parent of all in Linux - And because of this, as mentioned in [docker documentation](https://docs.docker.com/engine/reference/run/#foreground) :

    ```Note: A process running as PID 1 inside a container is treated specially by Linux: it ignores any signal with the default action. So, the process will not terminate on SIGINT or SIGTERM unless it is coded to do so.```

    This behaviour means that a dockerized application will not behave as you would expect on termination signals ( or what we consider as such ).

    Having an `init` process will solve this issue, by providing the expected behaviour on signals. Actually, this was so much of a common problem that, since Docker version 13, [tini](https://github.com/krallin/tini) was added as the command line argument `--init` to [docker](https://docs.docker.com/engine/reference/run/#specify-an-init-process).

    ```You can use the `--init` flag to indicate that an init process should be used as the PID 1 in the container. Specifying an init process ensures the usual responsibilities of an init system, such as reaping zombie processes, are performed inside the created container.```

    ```The default init process used is the first `docker-init` executable found in the system path of the Docker daemon process. This docker-init binary, included in the default installation, is backed by tini.```

    ```NOTE: If you are using Docker 1.13 or greater, Tini is included in Docker itself. This includes all versions of Docker CE. To enable Tini, just pass the --init flag to docker run.```

- Zombie process reaping

    When a process has started by a child to PID 1 process, lets call it a grandchild, and the child process exits, then PID 1 is supposed to adopt that grandchild ( and most likely terminate it... )

    If this actions does not happen, we are left with a process without an active parent, which we call `zombie process`.

    Without an init process in a container, we might start accumulating zombie processes, especially on web server environments, where worker processes trigger the creation of interpeter pools ( for example Apache -> php )

### Rust Libs used

- Singal-hook [Documentation](https://docs.rs/signal-hook/0.1.13/signal_hook/)

### License

- [APACHE 2.0](LICENSE)
