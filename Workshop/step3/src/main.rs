extern crate signal_hook;

use std::io::{Error, ErrorKind};
use std::{process, env};
use std::process::{Command, Stdio, exit};
use std::sync::Arc;
use std::sync::atomic::{AtomicBool, Ordering};

fn main() -> Result<(), Error> {

    let my_pid: u32 = process::id();
    println!("Hello, my process id is {}", my_pid);

    if my_pid == 1 {
        println!("I'm init !");
    }
    else {
        println!("I'm not init :(");
        //TODO : Specify how much of a problem this is for us
        //exit(0x1);
        println!("Will still start the process though...")
    }

    // Get the argument(s). Expecting the first process.
    let mut args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("No args left. please provide a process name...");
        exit(0x1);
    }

    // Remove ourselves from the argument list.
    // While Getting the process name we will use
    let process_names: Vec<String> =  args.drain(0..2).collect();

    // Get the process we want to start name.
    let proccess_name: String = process_names[1].to_string();
    
    println!("I will try to start `{}` with arguments {:?}", proccess_name, args);

    //TODO : Use stdout later
    //TODO : Check for timeout and handle it
    let _child_stdout = Command::new(proccess_name)
    .args(&args)
    .stdout(Stdio::piped())
    .stderr(Stdio::piped())
    .stdin(Stdio::null())
    .spawn()?
    .stdout
    .ok_or_else(|| Error::new(ErrorKind::Other,"Could not capture standard output."))?;

    println!("Started process... Waiting for signals");

    // Almost the same as sig_term = sig_inte = false
    let sig_term = Arc::new(AtomicBool::new(false));
    let sig_inte = Arc::new(AtomicBool::new(false));

    // Atomic booleans will now be pointed to the same memory location 
    // We are registering these here, so when signal hook changes them, we will know
    signal_hook::flag::register(signal_hook::SIGTERM, Arc::clone(&sig_term))?;
    signal_hook::flag::register(signal_hook::SIGINT, Arc::clone(&sig_inte))?;

    while !sig_term.load(Ordering::Relaxed) && !sig_inte.load(Ordering::Relaxed) {
        // Wait until someone triggers an nterupt.
    }

    // Interupt happened, before stopping, tell us which one
    println!("SIGINT: {:?}, SIGTERM: {:?}", sig_inte, sig_term);

    //TODO : Find a better way for this
    Ok(())
}
