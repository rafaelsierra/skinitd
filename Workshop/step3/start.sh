#!/bin/bash

echo "Building image..."
docker build . -t skinitd:step3
echo 
echo "Running, expecting our init process to start nginx and wait for SIGTERM or SIGKILL."
echo "You can kill by CTRL+C ( or your OS combination for SIGINT"
echo "Or running kill <pid> on the docker process"
echo 
echo "Try adding -d on the docker command as well"
docker run --rm -p "127.0.0.1:80:80/tcp" skinitd:step3
