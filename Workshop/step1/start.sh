#!/bin/bash

echo "Building image..."
docker build . -t skinitd:step1
echo 
echo "Running, expecting our entrypoint to be our init process."
docker run --rm skinitd:step1
echo
echo "Running again, adding --init on Docker run command..."
echo "expecting our entrypoint to not be our init process."
docker run --rm --init skinitd:step1
