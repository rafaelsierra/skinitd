# Workshop that explains how init processes work while using this project

## Description

After Reading the ( very well written ) guide about [how to write a minimal kernel in Rust](https://os.phil-opp.com/minimal-rust-kernel/). I decided to also try an write a minimal init process in Rust, using `qemu`.

After discussion with colleagues, it was obvious that enough interest existed to try to make it a step by step learning experience, so that we all benefit from this journey.

This, unfortunately meant that the project would go away from `qemu`, and replace it with Docker. But in the long run, it might be beneficial to all sides.

Disclaimer : This is a work in progress...

## Requirements

.Docker 13+ ( for using `--init` in examples)

## Steps

- Step1 : Just run your own init process, and check if it has `PID == 1`

- Step 2: Start a new process as an argument to your init process

  - Build nginx
  - Spawn a process - [Rust Link](https://rust-lang-nursery.github.io/rust-cookbook/os/external.html#continuously-process-child-process-outputs)

- Step 3: Start a long running process and Manage SIGINT and SIGTERM

  - Generate a valid `nginx.conf` [Githib Link](https://github.com/nginx/nginx/blob/master/conf/nginx.conf)
  - How to use Signals in Rust [ Rust Link ](https://rust-cli.github.io/book/in-depth/signals.html)
  - Signal-hook Library [Link](https://github.com/vorner/signal-hook)

- Step 4 : Reap zombies
  - TODO
