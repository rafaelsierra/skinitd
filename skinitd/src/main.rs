extern crate signal_hook;

use std::io::Error;
use std::process::{exit, Command, Stdio};
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::mpsc;
use std::sync::Arc;
use std::thread;
use std::time;
use std::{env, process};

const WAIT_DURATION: time::Duration = time::Duration::from_millis(10);

fn handle_signal(ch: mpsc::Receiver<bool>) -> mpsc::Receiver<Result<(), Error>> {
    let (tx, rx) = mpsc::channel();
    thread::spawn(move || {
        // Almost the same as sig_term = sig_inte = false
        let sig_term = Arc::new(AtomicBool::new(false));
        let sig_inte = Arc::new(AtomicBool::new(false));

        // Atomic booleans will now be pointed to the same memory location
        // We are registering these here, so when signal hook changes them, we will know
        signal_hook::flag::register(signal_hook::SIGTERM, Arc::clone(&sig_term)).unwrap();
        signal_hook::flag::register(signal_hook::SIGINT, Arc::clone(&sig_inte)).unwrap();
        loop {
            if sig_term.load(Ordering::Relaxed) {
                break;
            }
            if sig_inte.load(Ordering::Relaxed) {
                break;
            }
            if ch.try_recv().unwrap_or(false) {
                break;
            }
            thread::sleep(WAIT_DURATION);
        }
        // Interupt happened, before stopping, tell us which one
        println!("SIGINT: {:?}, SIGTERM: {:?}", sig_inte, sig_term);
        tx.send(Ok(())).unwrap();
    });
    return rx;
}

fn handle_process(
    ch: mpsc::Receiver<bool>,
    mut args: Vec<String>,
) -> mpsc::Receiver<Result<(), Error>> {
    let (tx, rx) = mpsc::channel();

    // Remove ourselves from the argument list.
    // While Getting the process name we will use
    let process_names: Vec<String> = args.drain(0..2).collect();

    // Get the process we want to start name.
    let proccess_name: String = process_names[1].to_string();

    println!(
        "I will try to start `{}` with arguments {:?}",
        proccess_name, args
    );

    let child = Command::new(proccess_name)
        .args(&args)
        .stdin(Stdio::null())
        .spawn();

    match child {
        Ok(mut child) => {
            // Process spawned successfuly
            // Starts a new thread to wait for it
            thread::spawn(move || {
                loop {
                    // Checks if the process must be killed
                    // TODO: Graceful killing?
                    if ch.try_recv().unwrap_or(false) {
                        // Received signal to exit
                        child.kill().unwrap();
                    }

                    // Checks if process is completed
                    match child.try_wait() {
                        Ok(None) => {
                            // Not complete yet
                        }
                        Ok(Some(status)) => {
                            // Completed
                            println!("Process completed with status {}", status);
                            tx.send(Ok(())).unwrap();
                            break;
                        }
                        Err(_) => {
                            // Something went wrong...
                        }
                    }
                    thread::sleep(WAIT_DURATION);
                }
            });
        }
        Err(e) => {
            tx.send(Result::Err(e)).unwrap();
        }
    }
    return rx;
}

fn main() -> Result<(), Error> {
    let my_pid: u32 = process::id();
    println!("Hello, my process id is {}", my_pid);

    if my_pid == 1 {
        println!("I'm init !");
    } else {
        println!("I'm not init :(");
        //TODO : Specify how much of a problem this is for us
        //exit(0x1);
        println!("Will still start the process though...")
    }

    // Get the argument(s). Expecting the first process.
    let args: Vec<String> = env::args().collect();
    if args.len() < 2 {
        println!("No args left. please provide a process name...");
        exit(0x1);
    }

    let (signal_tx, signal_rx) = mpsc::channel();
    let (proc_tx, proc_rx) = mpsc::channel();

    let signal_response = handle_signal(signal_rx);
    let proc_response = handle_process(proc_rx, args);

    loop {
        // Checks for system interruption
        match signal_response.try_recv() {
            Ok(_) => {
                // Got system interruption
                // Notifies process handler to exit
                proc_tx.send(true).unwrap();
                break;
            }
            Err(_) => {
                // Still waiting
            }
        }

        // Checks for the process output
        match proc_response.try_recv() {
            Ok(_) => {
                // Command is over
                // Notifies signal handler to exit
                signal_tx.send(true).unwrap();
                break;
            }
            Err(_) => {}
        }
        thread::sleep(WAIT_DURATION);
    }
    //TODO : Handle Zombie processes
    //TODO : Find a better way for this
    Ok(())
}
